package com.mickzeller.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@Entity
public class Award
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private @Setter String year;
    private @Setter String actress;
    private @Setter String movie;

    public Award()
    {
    }

}
