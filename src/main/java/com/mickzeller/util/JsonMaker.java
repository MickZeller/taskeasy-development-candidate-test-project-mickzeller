package com.mickzeller.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JsonMaker
{
    private String FILE_PATH = "C:\\Users\\mickz\\TaskEasy\\src\\main\\resources\\csv\\academy_award_actresses.csv";


    public String listAsJson()
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ReadFile readFile = new ReadFile(FILE_PATH);
        String json = gson.toJson(readFile.getInputList());
        return json;
    }
}
